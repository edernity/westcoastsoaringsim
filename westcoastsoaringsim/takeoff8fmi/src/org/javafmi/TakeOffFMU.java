package org.javafmi;

import org.javafmi.exporter.framework.FmiSimulation;
import org.javafmi.exporter.framework.Variable;
import org.javafmi.exporter.framework.modeldescription.ModelDescription;
import org.javafmi.exporter.framework.modeldescription.ModelDescriptionAttributes;
import org.javafmi.exporter.framework.modeldescription.ModelDescriptionSpec;
import org.javafmi.exporter.framework.modeldescription.ModelVariablesSpec;
import org.javafmi.exporter.framework.modeldescription.RealVariableSpec;
import org.javafmi.exporter.framework.modeldescription.ScalarVariableSpec;

/**
 * How to build see here:
 * 
 * https://bitbucket.org/siani/javafmi/wiki/JavaFmuBuilder
 * 
 * @author jru9fe
 */
public class TakeOffFMU extends FmiSimulation {

    private double pitch_dot, x_dot, targetXdot, trim_angle, control_angle;

    @Override
    public Status init() {
	register("pitch_dot", new Variable<Double>() {
	    @Override
	    public Double getValue() {
		return Double.valueOf(pitch_dot);
	    }

	    @Override
	    public Status setValue(Double value) {
		pitch_dot = value.doubleValue();
		return Status.OK;
	    }
	});
	register("x_dot", new Variable<Double>() {
	    @Override
	    public Double getValue() {
		return Double.valueOf(x_dot);
	    }

	    @Override
	    public Status setValue(Double value) {
		x_dot = value.doubleValue();
		return Status.OK;
	    }
	});
	register("targetXdot", new Variable<Double>() {
	    @Override
	    public Double getValue() {
		return Double.valueOf(targetXdot);
	    }

	    @Override
	    public Status setValue(Double value) {
		targetXdot = value.doubleValue();
		return Status.OK;
	    }
	});
	register("trim_angle", new Variable<Double>() {
	    @Override
	    public Double getValue() {
		return Double.valueOf(trim_angle);
	    }

	    @Override
	    public Status setValue(Double value) {
		trim_angle = value.doubleValue();
		return Status.OK;
	    }
	});
	register("control_angle", new Variable<Double>() {
	    @Override
	    public Double getValue() {
		return Double.valueOf(control_angle);
	    }

	    @Override
	    public Status setValue(Double value) {
		control_angle = value.doubleValue();
		return Status.OK;
	    }
	});

	return Status.OK;
    }

    @Override
    public Status doStep(double stepSize) {
	autopilot_internal();
	return Status.OK;
    }

    private void autopilot_internal() {
	double feedback;

	feedback = pitch_dot - (x_dot - targetXdot) / 100; // slowly adjust pitch angle to match target air speed
	if (pitch_dot < 0.0) { // pitching forward
	    if (trim_angle >= Math.toRadians(1.0)) {
		control_angle -= Math.toDegrees(feedback / 20); // quickly adjust controls to minimize pitching motions
	    } else {
		trim_angle -= feedback / 200;
	    }
	} else { // pitching back
	    if (control_angle <= 0.0) {
		if (trim_angle > Math.toRadians(-4.0))
		    trim_angle -= feedback / 200;
	    } else {
		control_angle -= Math.toDegrees(feedback / 20);
	    }
	}
	if (control_angle > 49.9)
	    control_angle = 49.9;
	else if (control_angle < 0.0)
	    control_angle = 0.0;
	if (trim_angle > Math.toRadians(1.0))
	    trim_angle = Math.toRadians(1.0);
	else if (trim_angle < Math.toRadians(-4.0))
	    trim_angle = Math.toRadians(-4.0);

    }

    @Override
    public Status reset() {
	return Status.OK;
    }

    @Override
    public ModelDescription createModelDescription() {
	return ModelDescription.from(new ModelDescriptionSpec(new ModelDescriptionAttributes("TakeOffFMU")) // <- Same as Class name
	    .withVariablesSpec(new ModelVariablesSpec().add(new RealVariableSpec(new ScalarVariableSpec("pitch_dot")))
		.add(new RealVariableSpec(new ScalarVariableSpec("x_dot")))
		.add(new RealVariableSpec(new ScalarVariableSpec("targetXdot")))
		.add(new RealVariableSpec(new ScalarVariableSpec("trim_angle")))
		.add(new RealVariableSpec(new ScalarVariableSpec("control_angle")))));
    }

    @Override
    public Status terminate() {
	/*
	 * Free resources, delete temporal
	 */
	return Status.OK;
    }
}
