////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Paraglider Glider Simulation and Visualization Program
// by Peter Spear. peter_spear@telus.net
// Nov 25, 2011
//
//    Copyright (C) 2011  Peter Spear
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as
//    published by the Free Software Foundation, either version 3 of the
//    License, or (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    To see a full copy of the GNU Affero General Public License
//    see <http://www.gnu.org/licenses/>.
//
//
// The physics of this simulation is as accurate as possible.
// Polar Data came from XFLR5 Foil analysis program.
//		http://xflr5.sourceforge.net/xflr5.htm
// The foil and glider design came from the GNULAB2 work of
// 	Pere H. Casellas Laboratori d'envol
//		http://www.laboratoridenvol.com/projects/gnuLAB2/gnuLAB2.en.html
// Big thanks to the Processing.js guys! What a great visualization tool!
//		http://processingjs.org/
// Beats hell out of my original C program that just spewed numbers.
//
// Feel free to modify and tweek this design as much as you like.
// Note that it took quite a bit to get the glider to stall and recover
// in a realistic manner. It is quite sensitive to values of the lift and
// drag coeficients. It is easy to break it. The XFLR5 polar data is only
// used between -16 degrees and +25 degrees aoa. Outside this range values
// for lift and drag were guessed at and tweeked.
//
// Further note to tweekers. Paragliders are almost pitch un-stable. Reduce the
// glider drag just little and the glider will become unstable. Play with it
// for a bit and you will soon give up on the "pendulum stability" idea.
//
// Have fun. Tune up your active flying, work on your infinite tumble and try a loop-de-loop
//

/* 
 * Copyright (c) 2015 Dennis Eder
 * This work is based on a paraglider simulation and visualization game by Peter Spear,
 * Director of the Westcoast Soaring Club in Canada, originally written in ProcessingJS 
 * (see http://www3.telus.net/cschwab/simPG/simGlider.pde), 
 * as announced here: http://www.paraglidingforum.com/viewtopic.php?t=43670 in 2011.
 * The goal is to port his game to JavaFX and use JavaFMI from SIANI/Spain to modularize 
 * pilot, glider and weather models showcasing FMI/FMU interoperability in a fun way.
 * It allows the user to design and exchange custom models and compare against others and even manual flight.
 * 
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *  
 *  To see a full copy of the GNU Affero General Public License
 *  see <http://www.gnu.org/licenses/>.
 *
 * Contributors: 
 *  Dennis Eder - port of Peter Spear's Javascript-based paraglider simulator (http://www3.telus.net/cschwab/simPG/simGlider.pde) to JavaFX, some rapid drafts to offer my team a debuggable environment and jumpstart into single-day hackfest
 */
package application;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Stack;

import application.flightdynamics.FlightDynamics;
import application.flightdynamics.FlightDynamics.AutopilotOut;
import dataplot.TabularDataPlot;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.transform.Affine;

public class Loop {
    public FlightDynamics flight;
    WeatherModel weather;
    final int LOOPS_PER_UPDATE = 1;
    final int UPDATES_PER_S = 60;
    final double TIME_STEP = 1 / ((double) LOOPS_PER_UPDATE) / ((double) UPDATES_PER_S); // simulation update rate in seconds.
    final double PIXEL_PER_M = 20;

    Image glider0;
    Image glider10;
    Image glider20;
    Image glider30;
    Image glider40;
    Image glider50;
    Image collapse;
    Image collapse2;
    Image halfbar;
    Image fullbar;

    final int lines = 2;
    int[] posY;
    int[] posX;

    Boolean pause = false;
   

    public static enum AutopilotMode {
	off, pg, fmu;
    }

    int i;
    int time = 0;
    int markTime = 0;
    double markX = 0.0;
    double markZ = 0.0; 
    int count = 0; 
    private DoubleProperty mouseXProperty = new SimpleDoubleProperty();
    private DoubleProperty mouseYProperty = new SimpleDoubleProperty();
    private boolean keyPressed;
    private Canvas canvas;
    private GraphicsContext gc; 
    private Canvas datacanvas;

    void initialize(Canvas canvas, Canvas datacanvas) throws IOException, URISyntaxException {
	this.canvas = canvas;
	this.datacanvas = datacanvas; 
	flight = new FlightDynamics();
	weather = new WeatherModel();
	flight.loadPolar();
	setupGlider(); 
	loadImages(new URI("/media/purple")); 
	posY = new int[lines];
	posX = new int[lines];

	for (i = 0; i < lines; i++) {
	    posY[i] = (int) (canvas.getHeight() / lines * i);
	    posX[i] = (int) (canvas.getWidth() / lines * i);
	}
	gc = canvas.getGraphicsContext2D();
	dataplot = new TabularDataPlot(datacanvas);
 
    }

    public void loadImages(java.net.URI dir) {
	String extension;
	if (new File(dir + "/glider0.gif").exists()){
	    extension = ".gif";
	}else{
	    extension = ".png";
	}
	glider0 = new Image(dir + "/glider0"+extension);
	glider10 = new Image(dir + "/glider10"+extension);
	glider20 = new Image(dir + "/glider20"+extension);
	glider30 = new Image(dir + "/glider30"+extension);
	glider40 = new Image(dir + "/glider40"+extension);
	glider50 = new Image(dir + "/glider50"+extension);
	collapse = new Image(dir + "/collapse"+extension);
	collapse2 = new Image(dir + "/collapse2"+extension);
	halfbar = new Image(dir + "/halfbar"+extension);
	fullbar = new Image(dir + "/fullbar"+extension);
	
    }

    public DoubleProperty mouseYProperty() {
	return mouseYProperty;
    }

    protected AutopilotOut pgAutopilotOutputs = new AutopilotOut();
    protected AutopilotOut fmuAutopilotOutputs = new AutopilotOut();

    protected TabularDataPlot dataplot;

    void drawTabular() {
	dataplot.clear();
	final double control_angle_min = 0.0;
	final double control_angle_max = 60.0;
	final double trim_angle_min = -5.0;
	final double trim_angle_max = 5.0;
	dataplot.figure(mouseYProperty.get(), 0, 400, "mouse y");
	dataplot.figure(pgAutopilotOutputs.control_angle, control_angle_min, control_angle_max, "pg  control");
	dataplot.figure(fmuAutopilotOutputs.control_angle, control_angle_min, control_angle_max, "fmu control");
	dataplot.figure(pgAutopilotOutputs.trim_angle, trim_angle_min, trim_angle_max, "pg  trim");
	dataplot.figure(fmuAutopilotOutputs.trim_angle, trim_angle_min, trim_angle_max, "fmu trim");
	dataplot.figure(flight.targetXdot, -30.0, 50.0, "targetXdot");
	dataplot.figure(flight.x_dot, -30.0, 50.0, "x_dot");
	dataplot.figure(flight.x_dot_dot, -30.0, 30.0, "x_dot_dot");
	dataplot.figure(flight.angle_of_attack, -30.0, 30.0, "angle_of_attack");
	dataplot.figure(flight.app_wind_angle, -180.0, 360.0, "app_wind_angle");
	dataplot.figure(flight.app_wind_angle, -180.0, 360.0, "app_wind_angle");
	dataplot.figure(flight.app_windX, -180.0, 180.0, "app_windX");
	dataplot.figure(flight.app_windZ, -180.0, 180.0, "app_windZ");
	dataplot.figure(flight.averageLD, -180.0, 180.0, "avgL/D");
	dataplot.figure(flight.cm_glider_length, -180.0, 180.0, "cm_glider_length");
	dataplot.figure(flight.cm_pilot_length, -180.0, 360.0, "cm_pilot_length");
	dataplot.figure(flight.cop_adjust_angle, -180.0, 360.0, "cop_adjust_angle");
	dataplot.figure(flight.drag_coef, -180.0, 360.0, "drag_coef");
	dataplot.figure(flight.eff_aoa_deg, -180.0, 360.0, "eff_aoa_deg");
	dataplot.figure(flight.form_drag, -100.0, 2000.0, "form_drag");
	dataplot.figure(flight.G, -180.0, 360.0, "G");
	dataplot.figure(flight.glider_drag, -100.0, 2000.0, "glider_drag");
	dataplot.figure(flight.glider_v, -180.0, 360.0, "glider_v");
	dataplot.figure(flight.glider_v_parallel, -180.0, 360.0, "glider_v_parallel");
	dataplot.figure(flight.glider_v_perp, -180.0, 360.0, "glider_v_perp");
	dataplot.figure(flight.glider_wind_angle, -180.0, 360.0, "glider_wind_angle");
	dataplot.figure(flight.glider_windX, -180.0, 360.0, "glider_windX");
	dataplot.figure(flight.glider_windZ, -180.0, 360.0, "glider_windZ");
	dataplot.figure(flight.induced_drag, -180.0, 360.0, "induced_drag");
	dataplot.figure(flight.induced_drag_coef, -180.0, 360.0, "induced_drag_coef");
	dataplot.figure(flight.lift, -2500.0, 2500.0, "lift");
	dataplot.figure(flight.lift_coef, -180.0, 360.0, "lift_coef");
	dataplot.figure(flight.line_drag, -180.0, 360.0, "line_drag");
	dataplot.figure(flight.line_length, -180.0, 360.0, "line_length");
	dataplot.figure(flight.line_tension, -100.0, 2000.0, "line_tension");
	dataplot.figure(flight.mass_glider, -180.0, 360.0, "mass_glider");
	dataplot.figure(flight.mass_pilot, -180.0, 360.0, "mass_pilot");
	dataplot.figure(flight.moment_of_inetia, -500.0, 500.0, "moment_of_inetia");
	dataplot.figure(flight.pilot_drag, -180.0, 360.0, "pilot_drag");
	dataplot.figure(flight.pitch_angle, -180.0, 360.0, "pitch_angle");
	dataplot.figure(flight.pitch_dot, -2.0, 2.0, "pitch_dot");
	dataplot.figure(flight.pitch_dot_dot, -2.0, 2.0, "pitch_dot_dot");
	dataplot.figure(flight.xCP, -180.0, 360.0, "xCP");
	dataplot.figure(flight.z, -180.0, 360.0, "z");
	dataplot.figure(flight.z_dot, -180.0, 360.0, "z_dot");
	dataplot.figure(flight.z_dot_dot, -180.0, 360.0, "z_dot_dot");
    }

    void draw() {
	gc.setTransform(new Affine());
	matrixStack.clear();
	double height = canvas.getHeight();
	double width = canvas.getWidth();
	gc.clearRect(0, 0, width, height);
	double mouseY = mouseYProperty.get();
	if (!pause) {
	    time++;
	    switch (flight.autoPilot) {
	    case fmu:
	    case pg:
		flight.autopilots(TIME_STEP, pgAutopilotOutputs, fmuAutopilotOutputs);
		break;
	    case off:
	    default:
		if (mouseY > ((double) (height / 2))) {
		    flight.trim_angle = Math.toRadians(1.0);
		    flight.control_angle = (mouseY - height / 2) / height * 99.9;
		} else {
		    flight.control_angle = 0.0;
		    flight.trim_angle = Math.toRadians((mouseY - height / 2) / ((double) (height / 2)) * 5.0 + 1.0);
		}
	    }
	    //if (time%10 == 0){
	    drawTabular();
	    //}
	    weather.step(UPDATES_PER_S);

	    for (i = 0; i < LOOPS_PER_UPDATE; i++) {
		flight.stepGlider(TIME_STEP, flight.control_angle, flight.trim_angle, weather.windX, weather.windZ);
	    }
	    flight.averageLD = 0.999 * flight.averageLD
		+ 0.001 * flight.lift / (flight.pilot_drag + flight.glider_drag); // average still air glide

	   

	    background();
	    stroke(1.0, 1.0, 1.0, 1.0);
	    for (i = 0; i < lines; i++) {
		double moved = flight.x_dot / UPDATES_PER_S * PIXEL_PER_M;
		posX[i] = (posX[i] - (int) moved);
		if (posX[i] > width)
		    posX[i] -= width;
		if (posX[i] < 0)
		    posX[i] += width;
		line(posX[i], 0, posX[i], height);

		moved = flight.z_dot / UPDATES_PER_S * PIXEL_PER_M;
		posY[i] = posY[i] - (int) moved;
		if (posY[i] > height)
		    posY[i] -= height;
		if (posY[i] < 0)
		    posY[i] += height;
		line(0, posY[i], width, posY[i]);
	    }
	    pushMatrix();
	    //translate(width / 2, 2 * height / 3);
	    translate(width / 2, height / 3);
	    rotate(-flight.pitch_angle);
	    if (flight.line_tension < -100.0)
		image(collapse2, 0, 0);
	    else if (flight.line_tension < 0.0)
		image(collapse, 0, 0);
	    else if (flight.control_angle <= 0) {
		if (Math.toDegrees(flight.trim_angle) > -1.3)
		    image(glider0, 0, 0);
		else if (Math.toDegrees(flight.trim_angle) > -2.6)
		    image(halfbar, 0, 0);
		else
		    image(fullbar, 0, 0);
	    } else if (flight.control_angle < 10)
		image(glider10, 0, 0);
	    else if (flight.control_angle < 20)
		image(glider20, 0, 0);
	    else if (flight.control_angle < 30)
		image(glider30, 0, 0);
	    else if (flight.control_angle < 40)
		image(glider40, 0, 0);
	    else
		image(glider50, 0, 0);
	    popMatrix();
	    if (weather.windMode > 0) { 
		drawWindVector();
	    }
	} 
    }

    private void drawWindVector() { 
	strokeRGBA255(75, 225, 255, 255);
	translate(50, 50);
	rotate(Math.atan2(weather.windZ, weather.windX));
	int wind = (int) (4 * Math.sqrt(weather.windX * weather.windX + weather.windZ * weather.windZ));
	line(0, 0, wind, 0);
	line(wind, 0, 0.85 * wind, -0.1 * wind);
	line(wind, 0, 0.85 * wind, 0.1 * wind);
    }

    private void strokeRGBA255(int r, int g, int b, int a) {
	stroke(r / 255.0, g / 255.0, b / 255.0, a / 255.0);

    }

    Stack<Affine> matrixStack = new Stack<>();

    private void popMatrix() {
	gc.setTransform(matrixStack.pop());

    }

    private void pushMatrix() {
	matrixStack.push(gc.getTransform());

    }

    private void translate(double x, double y) {
	gc.translate(x, y);
	//gc.translate(100, 100);
    }

    private void rotate(double angle) {
	gc.rotate(Math.toDegrees(angle));

    }

    private void stroke(double r, double g, double b, double a) {
	gc.setStroke(new Color(r, g, b, a));

    }

    public Loop() {
	super();
	FlightDynamics flight = new FlightDynamics();
	WeatherModel weather = new WeatherModel();
    }

    private void background() {
	gc.setFill(new Color(0.5, 0.5, 0.5, 1));
	gc.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

    }

    private void line(double x1, double y1, double x2, double y2) {
	gc.strokeLine(x1, y1, x2, y2);

    }

    private void image(Image img, int x, int y) {
	gc.drawImage(img, x, y);
    }

    //////////////////////////////////////////////////////////
    /**
     * You should only rely on the key char if the event is a key typed event.
     */
    public EventHandler<KeyEvent> createKeyEventHandler(Slider silder_targetXdot, ToggleButton toggle_data) {
	EventHandler<KeyEvent> handler = evt -> {
	    //KeyCode keycode = evt.getCode();
	    String key = evt.getCharacter();
	    if (key.equals("p") || key.equals("P")) {
		pause = !pause;
	    } else if (key.equals("a") || key.equals("A")) {
		silder_targetXdot.setValue(flight.app_windX);
	    } else if (key.equals("t") || key.equals("T")) {
		flight.pitch_dot = -6.0; // tumble!!
	    } else if (key.equals("r") || key.equals("R")) {
		flight.pitch_dot = 6.0; // reverse tumble!!
	    } else if (key.equals("d") || key.equals("D")) {
		toggle_data.setSelected(!toggle_data.isSelected());		 
	    } else if (key.equals("w") || key.equals("W")) {
		weather.windMode = (weather.windMode + 1) % 4;
		weather.windPhase = 0;
		printWindMode();
	    } else if (key.equals("+") || key.equals("=")) {
		if (weather.windSpeed < 20.0)
		    weather.windSpeed++;
		printWindSpeed();
	    } else if (key.equals("-") || key.equals("_")) {
		if (weather.windSpeed > 0.0)
		    weather.windSpeed--;
		printWindSpeed();
	    } else if (key.equals("[") || key.equals("{")) {
		weather.windPhase = 0;
		weather.windPeriod++;
		printWindFreq();
	    } else if (key.equals("]") || key.equals("}")) {
		weather.windPhase = 0;
		if (weather.windPeriod > 1.0)
		    weather.windPeriod--;
		printWindFreq();
	    } else if (key.equals("h") || key.equals("H")) {
		flight.mass_pilot += 20;
		setupGlider();
		printPilotMass();
	    } else if (key.equals("b") || key.equals("B")) {
		if (flight.mass_pilot > 20)
		    flight.mass_pilot -= 20;
		setupGlider();
		printPilotMass();
	    } else if (key.equals("g") || key.equals("G")) {
		flight.mass_glider++;
		setupGlider();
		printGliderMass();
	    } else if (key.equals("v") || key.equals("V")) {
		if (flight.mass_glider > 0)
		    flight.mass_glider--;
		setupGlider();
		printGliderMass();
	    } else if (key.equals("f") || key.equals("F")) {
		flight.line_length++;
		setupGlider();
		printLineLength();
	    } else if (key.equals("c") || key.equals("C")) {
		if (flight.line_length > 1)
		    flight.line_length--;
		setupGlider();
		printLineLength();
	    } else if (key.equals("m") || key.equals("M")) {
		printMark();
		markTime = time;
		markX = flight.x;
		markZ = flight.z;
	    } else if (key.equals(" ")) { //reset		
		flight.x_dot = 10.0;
		flight.z_dot = 1.0;
		flight.pitch_dot = 0.0;
		flight.x = 0.0;
		flight.z = 0.0;
		flight.pitch_angle = 0.0;
		weather.windX = 0.0;
		weather.windZ = 0.0;
		flight.mass_pilot = 80;
		flight.mass_glider = 4;
		flight.line_length = 7.0;
		weather.windSpeed = 5.0;
		weather.windMode = 0;
		setupGlider();
		System.out.println("Reset");
	    }
	};
	return handler;
    }

    void printMark() {	
	double deltaT = (time - markTime) / UPDATES_PER_S;
	System.out.println("Time(s) " + ff(deltaT, 5, 1) + ", Dist(m) " + ff(flight.x - markX, 5, 1)
	    + ", Height loss(m) " + ff(markZ - flight.z, 5, 1) + ", Speed(km/h) "
	    + ff(3.6 * (flight.x - markX) / deltaT, 5, 1) + ", Sink(m/s) " + ff((markZ - flight.z) / deltaT, 6, 2)
	    + ", Glide " + ff((flight.x - markX) / (flight.z - markZ), 6, 2));
	return;
    }

    String ff(double num, int len, int decimals) {
	double scale = Math.pow(10, decimals);
	StringBuilder t = new StringBuilder("" + Math.round(num * scale) / scale);
	while (t.length() < len)
	    t.append(" ");
	return t.toString();
    }

    void printWindMode() {	
	switch (weather.windMode) {
	case 0:
	    System.out.println("Calm");
	    break;
	case 1:
	    System.out.println("Wind vertical & sinusoidal");
	    break;
	case 2:
	    System.out.println("Wind horizontal & sinusoidal");
	    break;
	default:
	    System.out.println("Turbulent wind");
	    break;
	}
	return;
    }

    void printWindSpeed() {	
	System.out.println("Max wind speed: " + Math.round(weather.windSpeed) + " m/s");
	return;
    }

    void printWindFreq() {	
	System.out.println("Period of wind changes: " + Math.round(weather.windPeriod) + " s");
	return;
    }

    void printPilotMass() {	
	System.out.println("Pilot Mass: " + Math.round(flight.mass_pilot) + " kg");
	return;
    }

    void printGliderMass() {	
	System.out.println("Glider Mass: " + Math.round(flight.mass_glider) + " kg");
	return;
    }

    void printLineLength() {	
	System.out.println("Line Length: " + Math.round(flight.line_length) + " m");
	return;
    }

    /////////////////////////////////////////////////////////////////////////////////////
    void setupGlider() {

	flight.cm_glider_length = flight.line_length * flight.mass_pilot / (flight.mass_glider + flight.mass_pilot);
	flight.cm_pilot_length = flight.line_length - flight.cm_glider_length;
	flight.total_glider_mass = flight.GLIDER_VOLUME * weather.AIR_DENSITY + flight.mass_glider;
	flight.total_mass = flight.total_glider_mass + flight.mass_pilot;
	flight.moment_of_inetia = flight.total_glider_mass * flight.cm_glider_length * flight.cm_glider_length
	    + flight.mass_pilot * flight.cm_pilot_length * flight.cm_pilot_length;
	return;
    }

     
   

   

   
   

    //    /**
    //     * new member by Dennis
    //     */
    //    private double noise(double x, double y, double z) {
    //	return ImprovedNoise.noise(x, y, z);
    //    }

    /**
     * new member by Dennis
     * 
     * @throws IOException
     */
    public static RuntimeException uncheck(Throwable cause) {
	throw new RuntimeException(cause);
    }

}
